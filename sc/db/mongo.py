from collections import OrderedDict
import logging
import os
import sys

from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError

from sc.db.schema import MetadataDatabase


LOG = logging.getLogger(__name__)


# TODO we probably don't need these if we go with the env var URI
MONGODB_USER = 'root'
MONGODB_KEY = 'sciencecapsule'
MONGODB_HOST = 'logdb'
MONGODB_PORT = 27017


def _get_mongo_uri():
    LOG.debug('Searching for MongoDB URI')
    uri = None

    from sc.config import CONFIG_DIR

    uri_file_path = CONFIG_DIR / 'mongo_uri'

    if uri_file_path.exists():
        # TODO we should check that files are serialized/deserialized correctly
        uri_from_file = uri_file_path.read_text().strip()
        if uri_from_file:
            LOG.debug(f'Found MongoDB URI: "{uri_from_file}" from file: "{uri_file_path}"')
            # TODO we don't check that the URI is valid here, simply that it's not empty
            uri = uri_from_file
    else:
        LOG.info(f'File "{uri_file_path}" does not exist.')

    if uri is None:
        LOG.warning(
            'MongoDB URI is None! '
            'If the connection fails, it could signify a problem in the configuration,'
        )

    return uri


def _create_mongo_client(uri=None, timeout_s=3, **kwargs):
    # uri = os.getenv('SC_MONGO_URI', None)
    uri = uri or _get_mongo_uri()

    timeout_kwargs = {}
    timeoutMS = 1000 * timeout_s

    for kind in ['connect', 'serverSelection', 'socket', 'waitQueue']:
        timeout_kwargs[kind + 'TimeoutMS'] = timeoutMS

    if uri is not None:
        LOG.debug(f'Creating client from uri: "{uri}"')
        client = MongoClient(uri, **timeout_kwargs)
    else:
        LOG.debug('Creating client from kwargs:')
        # TODO should we still allow this method of creating a client,
        # where the caller can override the default settings?
        client = MongoClient(**kwargs, **timeout_kwargs)

    return client


def _test_mongo_client(client):
    return client.server_info()


_CLIENT = None


def get_mongo_client(**kwargs):

    global _CLIENT

    client = _CLIENT

    if client is not None:
        LOG.debug(f'Found existing client {client}. It will be used.')
    else:
        LOG.debug('Existing client not found. Will create a new client.')
        client = _create_mongo_client(**kwargs)

    try:
        LOG.debug('Testing if client connection to server is successful...')
        _test_mongo_client(client)
    except Exception as e:
        LOG.exception('Could not connect to the server')
        LOG.critical(
            'The program will now be terminated. '
            'Ensure that the MongoDB server is running, '
            'and the client connection parameters (uri_file, kwargs, etc) are set up properly.'
        )
        sys.exit(1)
    else:
        LOG.info(f'Connected to MongoDB client: {client}')
        _CLIENT = client

    return client


class Database(object):
    def __init__(self, host, port):
        self._host = host
        self._port = port

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port


class MongoStore(Database):
    def __init__(self):
        host = MONGODB_HOST
        port = MONGODB_PORT
        Database.__init__(self, host, port)
        self._db = None

    def connect(self, db):
        try:
            wait = 5  # 5ms server selection delay (will be moved to a config file)
            # client = MongoClient(host=self.host, port=self.port, username=MONGODB_USER, password=MONGODB_KEY, authMechanism='SCRAM-SHA-256', serverSelectionTimeoutMS=wait)
            # client = MongoClient(host=self.host, port=self.port, serverSelectionTimeoutMS=wait)
            client = get_mongo_client()
            client.server_info()
            self._db = client[db]
        except ServerSelectionTimeoutError as err:
            print("Unable to connect to server -- {}".format(err))
            sys.exit()

    def insert(self, collection_name, doc):
        # TODO: typecheck if `doc' is a valid dictionary
        collection = self._db[collection_name]
        collection.insert_one(doc)

    def bulk_insert(self, collection_name, docs):
        # TODO: typecheck if `docs' is a list of dictionary items
        collection = self._db[collection_name]
        collection.insert_many(docs)

    def query(self, collection_name, cond=None, proj=None, sort_field=None):
        collection = self._db[collection_name]
        results = [] # may be better to have a generator than an in-memory list
        if cond is None:
            if proj is None:
                cursor = collection.find()
            else:
                cursor = collection.find({}, proj)
            if sort_field is not None:
                cursor = cursor.sort(sort_field)
        else:
            if proj is None:
                cursor = collection.find(cond)
            else:
                cursor = collection.find(cond, proj)
            if sort_field is not None:
                cursor = cursor.sort(sort_field)
        for c in cursor:
            results.append(c)

        return results

    def truncate(self, collection_name):
        collection = self._db[collection_name]
        collection.drop()

    def distinct(self, collection_name, field, cond=None):
        collection = self._db[collection_name]
        results = []
        if cond is None:
            cursor = collection.distinct(field)
        else:
            cursor = collection.distinct(field, cond)
        for c in cursor:
            results.append(c)

        return results
