import platform
import sys
from pathlib import Path
import logging
import contextlib
import subprocess
import os
import configparser
import argparse
import yaml
import time
import collections
import signal


def _get_logger():
    logger = logging.getLogger('sc.bootstrap')
    logger.setLevel(logging.DEBUG)

    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter("%(asctime)s :: %(name)s :: %(levelname)s :: %(message)s"))
    logger.addHandler(handler)

    return logger


LOG = _get_logger()


class log_operation:

    _current_indent_level = -1

    def __init__(self, text):
        self.op_text = text
        type(self)._current_indent_level += 1

    @property
    def indent(self):
        return '    ' * int(type(self)._current_indent_level)

    def _log(self, msg):
        LOG.debug(f'{self.indent}{msg}')

    def __enter__(self):
        self._log(f'{self.op_text}...')
        return self._log

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if not exc_type:
            self._log(f'{self.op_text}: done')
        else:
            self._log(f'{self.op_text}: encountered errors')
        type(self)._current_indent_level -= 1


class Target:

    def __init__(self, system=None, has_root_access=None):

        if system is None:
            system = platform.system()

        self.system = system.title()
        self.has_root_access = has_root_access

    @property
    def is_windows(self):
        return self.system == 'Windows'

    @property
    def is_linux(self):
        return self.system == 'Linux'

    @property
    def is_macos(self):
        return self.system == 'Darwin'


TARGET = Target()


LOCALHOST_ADDR = '127.0.0.1'


def is_port_in_use(port):
    import socket

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        return s.connect_ex((LOCALHOST_ADDR, port)) == 0


class CmdArgs(collections.UserList):

    def __init__(self, *cmd_args):
        super().__init__(*cmd_args)

    def get_valid_arg(self, arg, escape_paths=None):
        if isinstance(arg, os.PathLike):
            out = os.fspath(arg)
            if escape_paths:
                if escape_paths in {"'", 'quote_single'}:
                    out = f"'{out}'"
                elif escape_paths in {'"', 'quote_double'}:
                    out = f'"{out}"'
                elif escape_paths == 'backslash':
                    out = out.replace('\\', '\\\\')
                else:
                    raise ValueError(f'Invalid value for "escape_paths": "{escape_paths}"')
        else:
            out = str(arg)

        return out

    def resolve_exe(self, exepath):
        import shutil

        match = shutil.which(exepath)

        if match:
            # the output needs to be a Path object for the escaping in get_valid_arg() to work
            return Path(match)
        else:
            LOG.warning(f'Requested PATH resolution for "{exepath}" but it was not found. Returning input value instead.')
            return exepath

    def to_list(self, resolve_exe=None, **kwargs):
        args = list(self.data)

        if resolve_exe:
            args[0] = self.resolve_exe(args[0])

        return [self.get_valid_arg(arg, **kwargs) for arg in args]

    def to_str(self, sep=' ', **kwargs):
        return str.join(sep, self.to_list(**kwargs))

    def __str__(self):
        return self.to_str()

    def without(self, *args_to_exclude):
        return type(self)([arg for arg in self if arg not in args_to_exclude])


class MongoLocalConfiguration:

    def __init__(self, dbpath, host=LOCALHOST_ADDR, port=27017, user_db_name='', auth_user=None, auth_key=None, exe_prefix_path=None):

        self.dbpath = Path(dbpath)

        self.host = host
        # if port is not given, search for a free port?
        self.port = port

        self.user_db_name = user_db_name

        self.auth = auth_user is not None
        self.auth_user = str(auth_user)
        self.auth_key = str(auth_key)

        self.exe_prefix_path = Path(exe_prefix_path) if exe_prefix_path else None

    @property
    def mongod_path(self):
        exe_name = 'mongod'
        if self.exe_prefix_path:
            return self.exe_prefix_path / exe_name
        return exe_name

    @property
    def mongod_cli_args(self):
        # args = [self.mongod_path]
        args = CmdArgs([self.mongod_path])

        args += ['--dbpath', self.dbpath]
        args += ['--bind_ip', self.host]
        args += ['--port', str(self.port)]

        if self.auth:
            args += ['--auth']

        return args

    @property
    def mongod_bootstrap_cli_args(self):
        return self.mongod_cli_args.without('--auth')

    @property
    def client_kwargs(self):
        timeout_ms = 5000

        timeout_kwargs = {
            'connectTimeoutMS': timeout_ms,
            'serverSelectionTimeoutMS': timeout_ms,
            'socketTimeoutMS': timeout_ms,
            'waitQueueTimeoutMS': timeout_ms
        }

        return dict(host=self.host, port=self.port, **timeout_kwargs)

    def add_auth_user(self):
        import pymongo

        with log_operation('Creating admin user to enable authentication') as log:
            with pymongo.MongoClient(**self.client_kwargs) as client:
                auth_db = client['admin']
                # log(f'client={client}')
                # log(f'client.server_info()={client.server_info()}')
                log(f'self.auth_user={self.auth_user}')
                log(f'self.auth_key={self.auth_key}')
                resp = auth_db.command("createUser", self.auth_user, pwd=self.auth_key, roles=["dbAdminAnyDatabase", "readWriteAnyDatabase"])
                log(f'resp={resp}')
                # auth_db.eval('db.shutdownServer(force: 1, timeoutSecs: 0)')
                # try:
                    # auth_db.command('shutdown', 1, timeoutSecs=1)
                # except pymongo.errors.AutoReconnect as e:
                    # print(e)

    @property
    def mongod_proc_kwargs(self):
        opts = {
            'close_fds': True,
        }

        if TARGET.is_windows:
            # this is needed to be able to use signal.CTRL_BREAK_EVENT to terminate the process gracefully
            opts['creationflags'] = subprocess.CREATE_NEW_PROCESS_GROUP

        return opts

    def terminate_gracefully(self, mongod_proc):
        if TARGET.is_windows:
            mongod_proc.send_signal(signal.CTRL_BREAK_EVENT)
        else:
            mongod_proc.terminate()

    def bootstrap_auth_instance(self):

        with log_operation('Start a temporary MongoDB instance to enable authentication') as log:
            mongod_args = self.mongod_bootstrap_cli_args

            log(f'Running command: {mongod_args}')
            proc = subprocess.Popen(mongod_args.to_list(), **self.mongod_proc_kwargs)

            with log_operation('Waiting for mongod process to finish initialization in the background'):
                time.sleep(5)
            returncode = proc.poll()

            log(f'mongod returncode={returncode}')
            if returncode in {None, 0}:
                log(f'mongod process (pid={proc.pid}) running without errors.')
                try:
                    self.add_auth_user()
                except Exception as e:
                    log('There was an error while adding auth credentials')
                    LOG.exception(e)
                else:
                    log('Auth credentials enabled successfully.')
                finally:
                    with log_operation('Terminating mongod process'):
                        self.terminate_gracefully(proc)

                        try:
                            proc.wait(timeout=3)
                        except KeyboardInterrupt:
                            log('This is just the same keyboard interrupt I sent there')
            else:
                log('The mongod process encountered an error.')
                # we won't have access to proc's stdout/err because its file descriptors are closed;
                # this seems to be necessary to have all of this working on Windows
                # error_msg = str.join('', proc.stderr.readlines())
                # out_msg = str.join('', proc.stdout.readlines())
                # print(f'error_msg=\n{error_msg}')
                # print(f'out_msg=\n{out_msg}')
                # raise subprocess.SubprocessError(str(mongod_args))
            with log_operation('Terminating mongod process'):
                self.terminate_gracefully(proc)

                try:
                    proc.wait(timeout=3)
                except KeyboardInterrupt:
                    log('This is just the same keyboard interrupt I sent there')

    @property
    def mongo_uri(self):
        return f'mongodb://{self.auth_user}:{self.auth_key}@{self.host}:{self.port}/{self.user_db_name}?authSource=admin&authMechanism=SCRAM-SHA-256'


class SupervisorServicesConfig:

    def __init__(self, base_dir=None):
        from collections import defaultdict

        if base_dir is None:
            base_dir = r'%(here)s'

        self.base_dir = Path(base_dir)
        self._data = defaultdict(dict)
        self._groups = defaultdict(list)

    def __getitem__(self, *args, **kwargs):
        return self._data.__getitem__(*args, **kwargs)

    def __setitem__(self, *args, **kwargs):
        self._data.__setitem__(*args, **kwargs)

    def _get_env_line(self, env, sep=','):
        "Convert an `env` mapping to a single line in the format required by Supervisor's `environment` key."

        kv_pairs = [f'{key}="{val}"' for key, val in env.items()]
        return str.join(sep, kv_pairs)

    def add_supervisor_internal_settings(self):
        supervisord = self['supervisord']
        supervisord['environment'] = 'SC_MANAGED_SERVICE=1'
        supervisord['logfile'] = self.base_dir / 'logs/supervisord.log'
        supervisord['pidfile'] = self.base_dir / 'supervisord.pid'

        self['rpcinterface:supervisor']['supervisor.rpcinterface_factory'] = 'supervisor.rpcinterface:make_main_rpcinterface'

    def add_http_server_unix(self):
        unix_socket_path = self.base_dir / 'supervisord.sock'
        self['unix_http_server']['file'] = unix_socket_path
        self['supervisorctl']['serverurl'] = f'unix://{unix_socket_path}'

    def add_http_server_inet(self, port, host=LOCALHOST_ADDR, auth_user=None, auth_key=None):
        host_port = f'{host}:{port}'

        server = self['inet_http_server']
        server['port'] = host_port

        client = self['supervisorctl']
        client['serverurl'] = f'http://{host_port}'

        if auth_user:
            for section in [server, client]:
                section['username'] = auth_user
                section['password'] = auth_key

    def add_http_server(self, socket_path=None, host=None, port=None, auth_user=None, auth_key=None):
        client = self['supervisorctl']

        if socket_path is not None:
            server = self['unix_http_server']
            server['file'] = socket_path
            client['serverurl'] = f'unix://{socket_path}'
        else:
            server = self['inet_http_server']
            host_port = f'{host}:{port}'
            server['port'] = host_port
            client['serverurl'] = f'http://{host_port}'

        if auth_user:
            for section in [server, client]:
                section['username'] = auth_user
                section['password'] = auth_key

    def add_to_group(self, group, name):
        self._groups[group].append(name)

    def get_command_line(self, args):

        args_out = CmdArgs(args)
        opts = {'resolve_exe': True}

        if TARGET.is_windows:
            opts.update(escape_paths='backslash', resolve_exe=True)

        return args_out.to_str(**opts)

    def add_program_settings(self, name, *cmd_args, group=None, environment=None, **kwargs):

        section = self[f'program:{name}']

        section['command'] = self.get_command_line(cmd_args)
        section['redirect_stderr'] = True
        section['stdout_logfile'] = self.base_dir / 'logs' / r'%(program_name)s.log'

        if environment:
            section['environment'] = self._get_env_line(environment)

        default_opts = dict(autostart=False, autorestart='unexpected')
        opts = dict(default_opts)

        if TARGET.is_windows:
            opts.update(stopsignal='CTRL_BREAK_EVENT')

        opts.update(kwargs)

        for key, val in opts.items():
            section[key] = val

        if group:
            self.add_to_group(group, name)

    def add_event_capture_settings(self, name='capture', **kwargs):
        cmd = ['python', '-m', 'sc.capture.cli']
        cmd += ['--monitor']

        self.add_program_settings(name, *cmd, **kwargs)

    @property
    def configparser(self):
        config = configparser.ConfigParser()

        for group_name, programs in self._groups.items():
            self[f'group:{group_name}']['programs'] = str.join(',', programs)

        config.read_dict(self._data)
        return config

    def write(self, file_obj=sys.stdout):

        # to match the format used in the examples
        opts = {'space_around_delimiters': False}

        self.configparser.write(file_obj, **opts)


PortInfo = collections.namedtuple('PortInfo', ['number', 'name', 'is_internal'])


class Initializer:

    def __init__(self, config_dir,
                 event_sources=None,
                 monitored_dirs=None, monitored_pids=None,
                 auth_user=None, auth_key=None
                 ):
        self.config_dir = Path(config_dir).absolute()

        self.monitored_dirs = [Path(d).absolute() for d in (monitored_dirs or [])]
        self.monitored_pids = [int(pid) for pid in (monitored_pids or [])]

        self.event_sources_spec = event_sources
        self.enabled_event_sources = []

        self.auth_user = auth_user
        self.auth_key = auth_key

        self._port_infos = []

    def create_directories(self, *dir_paths):
        for dir_ in dir_paths:
            dir_path = Path(dir_)
            with log_operation(f'Creating directory {dir_path}'):
                dir_path.mkdir(parents=True, exist_ok=True)

    @property
    def data_dir(self):
        return self.config_dir / 'data'

    @property
    def mongo_data_dir(self):
        return self.data_dir / 'mongo'

    @property
    def services_dir(self):
        return self.config_dir / 'services'

    @property
    def logs_dir(self):
        return self.services_dir / 'logs'

    @property
    def ports_by_number(self):
        "Return already assigned ports as a {number: port_info} mapping."
        return {port_info.number: port_info for port_info in self._port_infos}

    def get_open_port(self, name, port=54000, is_internal=True):
        # TODO check that it's open, if not, increment it
        port = int(port)
        n_attempts = 10
        attempt_counter = 0

        with log_operation(f'Looking for available port (starting from {port})') as log:
            while attempt_counter <= n_attempts:
                attempt_counter += 1
                log(f'Attempt {attempt_counter}/{n_attempts}')
                if port in self.ports_by_number:
                    port_info = self.ports_by_number[port]
                    log(f'Port {port} already assigned to name "{port_info.name}".')
                    port += 1
                # TODO in what ways could is_port_in_use() fail?
                elif is_port_in_use(port):
                    log(f"Port {port} is not available. Incrementing port number and trying again.")
                    port += 1
                else:
                    log(f"Port {port} is available.")
                    break
            else:
                msg = f"Could not find an available port after {attempt_counter} attempts."
                log(msg)
                # TODO what should happen at this point? raise?
                raise ValueError(msg)

            log(f'Saving port {port} with name: "{name}"')

            port_info = PortInfo(port, name, is_internal=is_internal)
            self._port_infos.append(port_info)

        return port

    def is_valid_unix_socket(self, addr):
        import socket

        with log_operation(f'Testing if UNIX socket with address {addr} is supported') as log:
            try:
                socket.socket(socket.AF_UNIX, socket.SOCK_STREAM).bind(os.fspath(addr))
            except Exception as e:
                log(f'Could not bind to socket: {e}')
            else:
                log('Test was successful')
                # FIXME this is only valid for a file-based socket
                Path(addr).unlink()
                return True

        return False

    def init_directories(self):
        if self.config_dir.exists():
            LOG.critical(f'Directory {self.config_dir} already exists. Remove it manually before trying again.')
            sys.exit(1)

        LOG.info(f'Directory {self.config_dir} does not exist; it will be created.')
        self.create_directories(self.config_dir, self.data_dir, self.services_dir)

    def init_event_sources(self):
        spec = self.event_sources_spec
        separator = ','

        with log_operation('Setting event sources') as log:
            if spec:
                log(f'Event sources specified: "{spec}"')
                if spec.lower() == 'auto':
                    log('Will infer available sources based on the current target.')
                    event_sources = self.infer_available_event_sources()
                else:
                    event_sources = spec.split(separator)
                    log(f'Assigning event source(s): {event_sources}')
            else:
                log('No event sources specified. No event source will be assigned.')
                event_sources = []

            self.enabled_event_sources = event_sources
            log(f'Enabled event sources: {self.enabled_event_sources}')

    def init_mongo(self):
        self.create_directories(self.mongo_data_dir)
        port = self.get_open_port('mongo')
        default_user_db_name = 'sciencecapsule'
        self.mongo = MongoLocalConfiguration(
            self.mongo_data_dir,
            port=port,
            user_db_name=default_user_db_name,
            auth_user=self.auth_user,
            auth_key=self.auth_key
        )
        with log_operation('Preparing local MongoDB instance'):
            self.mongo.bootstrap_auth_instance()

        mongo_uri_file_path = self.config_dir / 'mongo_uri'

        with log_operation(f'Writing Mongo URI to file {mongo_uri_file_path}'):
            with mongo_uri_file_path.open('w') as f:
                f.write(self.mongo.mongo_uri)

    @property
    def webui_server_cmd_args(self):
        path_this = Path(__file__)
        path_ui = path_this.parent.parent / 'ui'
        path_webui_server = path_ui / 'backend' / 'server.js'

        return 'node', path_webui_server

    def infer_available_event_sources(self):
        sources = []

        with log_operation('Inferring event sources for filesystem events') as log:
            if TARGET.is_linux:
                fs_source = 'inotify'
                log(f'Target is linux, so source "{fs_source}" can be used.')
            else:
                fs_source = 'watchdog'
                log(f'Source "{fs_source}" supported for this target.')

            sources.append(fs_source)

        with log_operation('Inferring event sources for OS process events') as log:
            # TODO at the moment `has_root_access` is always falsey since strace is not well tested
            if TARGET.is_linux and TARGET.has_root_access:
                proc_source = 'strace'
                log(f'Target is linux and has root access, so source "{proc_source}" can be used.')
                sources.append(proc_source)
            else:
                log(f'This type of event source is not currently supported for this target.')

        return sources

    @property
    def default_events_config(self):
        d = {}

        d['monitored_dirs'] = self.monitored_dirs

        d['sources'] = list(self.enabled_event_sources)

        return d

    def create_events_config(self):
        config = self.default_events_config

        # TODO this is necessary for the YAML serialization,
        # but maybe there are cleaner ways to do so using yaml.dump() options
        config['monitored_dirs'] = [os.fspath(p) for p in config['monitored_dirs']]

        events_file_path = self.config_dir / 'events.yml'
        with log_operation(f'Writing events configuration to file {events_file_path}'):
            with events_file_path.open('w') as f:
                yaml.dump(config, f)

    def create_supervisor_config(self):
        self.create_directories(self.services_dir, self.logs_dir)

        conf = SupervisorServicesConfig()

        with log_operation('Creating configuration for services management (Supervisor)') as log:
            conf.add_supervisor_internal_settings()

            server_opts = {'auth_user': self.auth_user, 'auth_key': self.auth_key}

            socket_path = self.services_dir / 'supervisor.sock'
            if self.is_valid_unix_socket(socket_path):
                server_opts['socket_path'] = socket_path
            else:
                port = self.get_open_port('supervisor')
                server_opts['host'] = LOCALHOST_ADDR
                server_opts['port'] = port

            log(f'Supervisor internal server opts: {server_opts}')
            conf.add_http_server(**server_opts)

            conf.add_program_settings('mongo', *self.mongo.mongod_cli_args, priority=1)

            for source_name in self.enabled_event_sources:
                with log_operation(f'Setting service configuration for enabled event source: "{source_name}"'):
                    conf.add_event_capture_settings()

            with log_operation('Setting service configuration for WebUI server'):
                port = self.get_open_port('webui', is_internal=False)
                conf.add_program_settings('webui_server', *self.webui_server_cmd_args, environment={'SC_UI_PORT': port})

        supervisor_config_file_path = self.services_dir / 'supervisor.conf'

        with log_operation(f'Saving services configuration to file {supervisor_config_file_path}'):
            with supervisor_config_file_path.open('w') as f:
                conf.write(f)

    def display_port_info(self):
        items = self._port_infos
        print(f'{len(items)} port(s) were set during this setup.')
        for port_info in items:
            text = f'{port_info.number}\t{port_info.name}'
            if port_info.is_internal:
                text += '\t(internal)'
            print(text)

    def display_activation_steps(self):
        # TODO specialize according to target (e.g. using SET for windows, etc)
        msg = f"""

Setup completed for directory {self.config_dir}!

To use Science Capsule, first make sure that the environment variable "SC_CONFIG_DIR" is set:

    export SC_CONFIG_DIR="{self.config_dir}"\t\t(Bash, Zsh, and other Bash-like shells)
    SET SC_CONFIG_DIR="{self.config_dir}"\t\t(Windows)

Then, to start the services, run:

    sc services start all

        """

        print(msg)

    def run(self):
        steps = [
            self.init_directories,
            self.init_event_sources,
            self.init_mongo,
            self.create_supervisor_config,
            self.create_events_config,
            self.display_activation_steps,
            self.display_port_info,
        ]

        for step in steps:
            step_msg = f'Running step "{step.__qualname__}"'

            with log_operation(step_msg):
                step()


def run(*args, **kwargs):
    initializer = Initializer(*args, **kwargs)
    initializer.run()


def populate_cli_parser(parser):

    parser.add_argument('config_dir', help="""
        The directory that will be created to contain the configuration.
        To avoid overwriting existing directories, specifying an existing directory is not allowed."""
    )
    parser.add_argument('-d', '--monitored-dir', metavar='DIR', dest='monitored_dirs', action='append', help="""
        Directory to be monitored. For multiple directories, specify this option multiple times,
        e.g. "-d /path/to/dir1 -d /path/to/dir2"
        """
    )
    # parser.add_argument('-p', '--monitored-pid', metavar='PID', dest='monitored_pids', action='append',
    #     help="""
    #     Process IDs to be monitored. For multiple PIDs, specify this option multiple times,
    #     e.g. "-p 1234 -p 5678".
    #     Process monitoring is only available on Linux systems and may require additional configuration.
    #     Refer to the documentation for more information.
    #     """
    # )
    parser.add_argument('-s', '--event-sources', dest='event_sources', default='auto', help="""
        Event source(s) to use, separated by comma for multiple sources,
        e.g. "--event-sources sourceA,sourceB".
        If "auto" (default), event sources supported by the current target will be chosen automatically.
        """
    )

    default_auth_user = 'sciencecapsule'
    default_auth_key = 'sciencecapsule'

    def _dispatch(args):
        return run(
            args.config_dir,
            monitored_dirs=args.monitored_dirs,
            # monitored_pids=args.monitored_pids,
            event_sources=args.event_sources,
            auth_user=default_auth_user,
            auth_key=default_auth_key
        )

    parser.set_defaults(dispatch_func=_dispatch)

    return parser


def main():

    parser = argparse.ArgumentParser()
    populate_cli_parser(parser)

    args = parser.parse_args()
    args.dispatch_func(args)

if __name__ == "__main__":
    main()
