## Using Science Capsule bare-metal (without containers)

### Installation

- These steps were successfully tested on:
    - Linux (Ubuntu 18.04.4 and 20.04)
    - macOS (10.12.6)
    - Windows (10.0.10240)

#### Prerequisites

The following tools should be available on the system to be able to install and run Science Capsule:

- `conda` package manager
- Git
- (Linux only, optional) `inotifywait` (generally distributed in a package together with related utilties, e.g. `inotifytools`)
- (Linux only, optional) `strace`
    - Enabling `strace` for Science Capsule might require additional steps. Refer to [this document](./docs/enabling-strace.md) for a detailed description.

#### Science Capsule repository

Clone the `sciencecapsule` repository and enter the `sciencecapsule` directory:

```sh
git clone git@bitbucket.org:sciencecapsule/sciencecapsule
cd sciencecapsule
```

#### Set up environment and install packages

From the root directory of the repository, install the dependencies in a Conda environment called `sciencecapsule`:

```sh
conda env create --file dependencies/bare-metal.yml
```

**NOTE** If the Conda environment already exists, re-run this command adding the `--force` option, or manually remove the environment with `conda env remove --name sciencecapsule` beforehand.

Activate the `sciencecapsule` environment.
Once the environment is active, its name will appear on the shell prompt.
All following steps assume that the Conda environment has been activated.

```sh
conda activate sciencecapsule
(sciencecapsule)
```

Install the `sc` Python package:

```sh
python -m pip install --editable .
```

Install the Node.js dependencies using npm:

```sh
cd ui/backend
npm install
cd -
```

#### Set up config directory

The `sc bootstrap` command automatically creates all necessary configuration.

Its main argument is `config_dir`, the path to the configuration directory that will be created and populated during the initialization process.
To avoid overwriting existing configurations, the directory must not already exist.
To re-run the initialization on an existing directory, delete it manually before running `sc bootstrap`.

The following `sc bootstrap` usage examples assume that the configuration directory to initialize is `$HOME/.scicap`.

With the single positional argument, the initialization will not pre-populate monitored directories.
The monitored directories can be added after the initialization is complete by editing the `events.yml` file in the newly created config directory.

```sh
sc bootstrap "$HOME/.scicap"
```

By specifying the `-d/--monitored-dir` option one or more times, they will be added to the `events.yml` file during the initialization process:

```sh
# monitored directories should exist before the monitoring services are started
mkdir -p $HOME/workdir /tmp/test
sc bootstrap "$HOME/.scicap" --monitored-dir "$HOME/workdir" --monitored-dir /tmp/test
```

The `-s/--event-sources` option is used to set the enabled event sources in `events.yml`.
If not given, the default value `auto` will result in the best supported sources being chosen depending on the target platform where the script is running.
Currently, in practice, this means enabling `inotify` on Linux, and `watchdog` on macOS and Windows:

```sh
# this is equivalente to the previous command
sc bootstrap "$HOME/.scicap" --monitored-dir "$HOME/workdir" --monitored-dir /tmp/test --event-sources auto
```

One or more values can be specified as a comma-separated list.
In this case, the chosen sources will be added to the configuration files regardless if they are available on the target platform or not.

```sh
# running this on macOS or Windows will produce valid configuration files, but the corresponding services will crash upon startup
sc bootstrap "$HOME/.scicap" --monitored-dir "$HOME/workdir" --monitored-dir /tmp/test --event-sources inotify,strace
```

If the initialization setup finishes without errors, a message will show the steps needed to use Science Capsule using the newly created configuration directory,
as well as the ports assigned to the various services.
As long as `SC_CONFIG_DIR` is set, the `sc` commands can be issued from any directory. For testing the correct installation, you can either run the [test suite](docs/testing.md), or run the [example](docs/example.md).

### Usage

- When opening e.g. a new terminal, make sure that:
    - The `sciencecapsule` Conda env is activated,
    - The `SC_CONFIG_DIR` environment variable is set to a value that matches the one used for the initialization

#### Change logging level

Set the `SC_LOGGING_LEVEL` environment variable to any of `CRITICAL`, `WARNING`, `INFO`, `DEBUG` (the default).

#### `sc services` commands

At the moment `sc services` is a very thin wrapper around `supervisorctl`, so the supported actions/arguments are the same:

```sh
sc services {start/stop/restart} <service-name>
sc services {start/stop/restart} all

# stop all services as well as the `supervisord` process
sc services shutdown

sc services status

# clear logs for one or more services
sc services clear <service-name>
sc services clear all

# show a list of possible actions supported by the underlying supervisorctl command
sc services help

# without any argument, an interactive session will be started, supporting the same actions
sc services
```

Vanilla Supervisor resources such as e.g. `supervisorctl --help` can be used for more information.

Running `sc services` with the `--inspect-processes` flag will show all processes created by Science Capsule.
This does not rely on Supervisor, and thus can be used to e.g. verify that no orphan processes are left behind at the end of a session.

```sh
sc services --inspect-processes
```

##### Dependencies between services

If using `sc services` commands to manage individual services, keep in mind that the `mongo` services must be running for other services to work properly.

In other words, assuming that these commands are run from a fresh start, e.g. after `sc services shutdown`, the following will not work:

```sh
# The `capture` service requires the `mongo` service to be running
sc services start capture
```

But any of these will:

```sh
sc services start all
sc services start mongo capture
```

#### Enabling capture of process events with strace on bare-metal (Linux only)

**IMPORTANT**  Enabling `strace` might require additional steps. Refer to [this document](./docs/enabling-strace.md) for a detailed description.

On compatible platforms, Science Capsule can capture OS process events through the Strace command-line tool available on Linux-based distributions.

To enable process event capture, open the configuration file located at `$SC_CONFIG_DIR/events.yml` and set or verify the following settings:

- `monitored_pids` is a list containing the PIDs of the process to be monitored
    - The monitoring is recursive, i.e. all descendants of each monitored process will be monitored as well
- `sources` contains the `strace` event capture source

After editing the file, restart the capture service if it was already running:

```sh
sc service restart capture
```
