#!/bin/bash

log-msg () {
    local _msg="$1"
    echo "[entrypoint.sh] $_msg"
}

reset-event-sources() {
    local _config_file="$SC_CONFIG_DIR/events.yml"

    # NOTE a better way to do this would be to filter out strace while keeping the original sources
    # in practice, resetting the sources to inotify should cover the base case
    # until we have a proper way of setting config values
    log-msg "Resetting event sources to: [inotify]"
    yq write --inplace "$_config_file" 'sources' '[]'
    # --prettyPrint is used to ensure a consistent formatting
    # e.g. lists displayed with one element per line rather than inline
    yq write --inplace --prettyPrint "$_config_file" 'sources[+]' 'inotify'

    log-msg "Config file \"$_config_file\" after updates:"
    cat $_config_file
}


add-strace-source () {
    local _config_file="$SC_CONFIG_DIR/events.yml"
    local _pid_to_monitor="$1"

    log-msg "Removing stale list of monitored PIDs"
    yq write --inplace "$_config_file" 'monitored_pids' '[]'
    log-msg "Adding current PID to list of monitored PIDs"
    yq write --inplace --prettyPrint "$_config_file" 'monitored_pids[+]' "$_pid_to_monitor"
    log-msg "Adding 'strace' to event sources"
    yq write --inplace --prettyPrint "$_config_file" 'sources[+]' 'strace'

    log-msg "Config file \"$_config_file\" after updates:"
    cat $_config_file

}

check-strace () {
    local _no_strace_msg="Event source strace will not be enabled. Event capture will start normally, but process events will not be captured by default."

    reset-event-sources

    if $( capsh --print | grep -q sys_ptrace ); then
        log-msg "The container was created with the sys_ptrace capability."
        local _kernel_ptrace_scope=$( sysctl --values kernel.yama.ptrace_scope )
        if [ $_kernel_ptrace_scope -eq 0 ]; then
            log-msg "ptrace_scope has correct value"
            THIS_PID=$$
            log-msg "The PID of the starting process is $THIS_PID"
            log-msg "Attempting to adding strace to the source monitoring current PID"
            add-strace-source $THIS_PID
        else
            log-msg "Warning! ptrace is not enabled at the kernel level."
            log-msg "(value of kernel.yama.ptrace_scope is: $_kernel_ptrace_scope; required value: 0)"
            log-msg "$_no_strace_msg"
        fi
    else
        log-msg "sys_ptrace not among the capabilities; skipping strace setup"
        log-msg "$_no_strace_msg"
    fi
}

on-startup () {
    type sc-bootstrap
    type sc

    [ -d $SC_CONFIG_DIR ] && echo "Config dir found: $SC_CONFIG_DIR" || sc-bootstrap "$SC_CONFIG_DIR" --monitored-dir "$HOME"

    sc services start all
}

log-msg "Container environment:"
env

if [ -z $SC_NO_AUTOSTART ]; then
    check-strace
    # this log level will affect the services
    export SC_LOG_LEVEL=INFO
    sc services start all
    # this log level will affect the `sc` CLI tool
    export SC_LOG_LEVEL=INFO
else
    log-msg "SC_NO_AUTOSTART=$SC_NO_AUTOSTART"
    log-msg "Science Capsule services will not be started automatically."
fi

exec "$@"
